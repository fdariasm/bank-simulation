# --- !Ups

CREATE TABLE cuenta
(
  id serial NOT NULL,
  numero character varying(20) NOT NULL,
  saldo numeric NOT NULL DEFAULT 0,
  id_cliente integer NOT NULL,
  CONSTRAINT pk_cuenta PRIMARY KEY (id),
  CONSTRAINT fk_cliente_cuenta FOREIGN KEY (id_cliente)
      REFERENCES cliente (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

# --- !Downs

DROP TABLE cuenta;
