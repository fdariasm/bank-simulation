# --- !Ups

CREATE TABLE cliente
(
  nombre character varying(100) NOT NULL,
  direccion character varying(200) NOT NULL,
  telefono character varying(50),
  id serial NOT NULL,
  CONSTRAINT pk_cliente PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE cliente;