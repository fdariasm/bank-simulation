# --- !Ups

CREATE TABLE movimiento
(
  id serial NOT NULL,
  tipo integer NOT NULL,
  fecha timestamp without time zone NOT NULL,
  valor numeric NOT NULL,
  id_cuenta integer NOT NULL,
  CONSTRAINT pk_movimiento PRIMARY KEY (id),
  CONSTRAINT fk_movimiento_cuenta FOREIGN KEY (id_cuenta)
      REFERENCES cuenta (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

# --- !Downs

DROP TABLE movimiento;