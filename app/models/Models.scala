package models

import java.util.Date

case class Cliente(
    nombre: String, 
    direccion: String,
    telefono: String,
    id: Option[Int] = None)
    
case class Cuenta(
    numero: String,
    saldo: Double,
    idCliente: Int,
    id: Option[Int] = None)
    
case class Movimiento(
    tipo: TipoMovimiento.TipoMovimiento,
    fecha: Date,
    valor: Double,
    idCuenta: Int,
    id: Option[Int] = None)

case class Reporte(
    cuenta: Cuenta,
    totalDebitos: Double,
    totalCreditos: Double
    )

object TipoMovimiento extends Enumeration{
  type TipoMovimiento = Value
  val debito, credito = Value
}
    