package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.MessagesApi
import play.api.i18n.I18nSupport
import play.api.libs.concurrent.Execution.Implicits._
import javax.inject.Inject
import services.ClientesService
import models.Cliente
import play.api.i18n.Messages
import scala.concurrent.Future
import services.CuentasService
import services.MovimientosService
import models.TipoMovimiento
import models.Cuenta

class MovimientosController @Inject() (
    val movimientosService: MovimientosService,
    val cuentasService: CuentasService,
    val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val movForm = Form(
    tuple(
      "tipo" -> number,
      "valor" -> number))

  def mostrarMovimientos(idCliente: Int, idCuenta: Int) = Action.async { implicit rs =>
    runWithCuenta(idCliente, idCuenta) { cuenta =>
      movimientosService.movimientosCuenta(cuenta)
        .map { mov => Ok(views.html.movimientos(idCliente, cuenta, mov)) }
    }
  }

  def runWithCuenta(idCliente: Int, idCuenta: Int)(
    f: (Cuenta => Future[Result]))(
      implicit rs: Request[AnyContent]): Future[Result] = {
    cuentasService.getCuenta(idCuenta).flatMap {
      case None => Future.successful(Redirect(routes.CuentasController.mostrarCuentas(idCliente))
        .flashing("error" -> Messages("cuenta.invalida")))
      case Some(cuenta) => f(cuenta)
    }
  }

  def registrarMovimiento(idCliente: Int, idCuenta: Int) = Action.async { implicit rs =>
    runWithCuenta(idCliente, idCuenta) { cuenta =>
      movForm.bindFromRequest.fold(
        fwe => Future.successful(Redirect(routes.MovimientosController.mostrarMovimientos(idCliente, idCuenta))
          .flashing("error" -> Messages("movimiento.valor"))),
        {
          case (tipo, valor) =>
            movimientosService.registrarMovimiento(cuenta, TipoMovimiento.apply(tipo), valor) match {
              case Left(msg) =>
                Future.successful(Redirect(routes.MovimientosController.mostrarMovimientos(idCliente, idCuenta))
                  .flashing("error" -> msg))
              case Right(futMov) =>
                futMov.map { _ =>
                  Redirect(routes.MovimientosController.mostrarMovimientos(idCliente, idCuenta))
                    .flashing("success" -> Messages("movimiento.registrado"))
                }
            }
        })
    }
  }
}