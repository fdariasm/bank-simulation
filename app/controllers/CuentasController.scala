package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.MessagesApi
import play.api.i18n.I18nSupport
import play.api.libs.concurrent.Execution.Implicits._
import javax.inject.Inject
import services.ClientesService
import models.Cliente
import play.api.i18n.Messages
import scala.concurrent.Future
import services.CuentasService
import java.util.Date
import org.joda.time.LocalDate

class CuentasController @Inject() (
    val clientesService: ClientesService,
    val cuentasService: CuentasService,
    val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val reporteForm = Form(
    tuple(
      "desde" -> date,
      "hasta" -> date))

  def mostrarCuentas(idCliente: Int) = Action.async { implicit rs =>
    runWithCliente(idCliente) {
      cliente =>
        cuentasService.cuentasCliente(cliente)
          .map(cuentas => Ok(views.html.cuentas(cliente, cuentas)))
    }
  }

  def agregarCuenta(idCliente: Int) = Action.async { implicit rs =>
    runWithCliente(idCliente) { cliente =>
      cuentasService.crearCuenta(cliente)
        .map { cuenta =>
          Redirect(routes.CuentasController.mostrarCuentas(idCliente))
            .flashing("success" -> Messages("cuenta.creada", cuenta.numero))
        }
    }
  }

  def runWithCliente(idCliente: Int)(f: (Cliente => Future[Result]))(implicit rs: Request[AnyContent]): Future[Result] = {
    clientesService.getCliente(idCliente).flatMap {
      case None          => Future.successful(Redirect(routes.ClientesController.clientes()).flashing("error" -> Messages("cliente.invalido")))
      case Some(cliente) => f(cliente)
    }
  }

  def eliminar(idCliente: Int, id: Int) = Action.async { implicit rs =>
    cuentasService.getCuenta(id).flatMap {
      case None => Future.successful(Redirect(routes.ClientesController.clientes()).flashing("error" -> Messages("cuenta.invalida")))
      case Some(_) =>
        cuentasService.eliminarCuenta(id).map { _ =>
          Redirect(routes.CuentasController.mostrarCuentas(idCliente))
            .flashing("success" -> Messages("cuenta.eliminada"))
        }
    }
  }

  def reporte(idCliente: Int) = Action.async { implicit rs =>
    runWithCliente(idCliente) { cliente =>
      val unMes = new LocalDate().minusMonths(1).toDate
      val today = new Date
      cuentasService.verReporte(cliente, unMes, today).map { reportes =>
        Ok(views.html.reporte(
          cliente, reportes, reporteForm.fill((unMes, today))))
      }
    }
  }

  def verReporte(idCliente: Int) = Action.async { implicit rs =>
    runWithCliente(idCliente) { cliente =>
      reporteForm.bindFromRequest.fold(
        fwe => Future.successful(BadRequest(views.html.reporte(
          cliente, Seq(), fwe))),
        {
          case (desde, hasta) =>
            cuentasService.verReporte(cliente, desde, hasta).map { reportes =>
              Ok(views.html.reporte(
                cliente, reportes, reporteForm.fill((desde, hasta))))
            }
        })
    }
  }

}