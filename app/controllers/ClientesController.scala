package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.MessagesApi
import play.api.i18n.I18nSupport
import play.api.libs.concurrent.Execution.Implicits._
import javax.inject.Inject
import services.ClientesService
import models.Cliente
import play.api.i18n.Messages
import scala.concurrent.Future

class ClientesController @Inject() (
    val clientesService: ClientesService,
    val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val clienteForm = Form(
    mapping(
      "nombre" -> nonEmptyText,
      "direccion" -> nonEmptyText,
      "telefono" -> nonEmptyText.verifying(Messages("formato.telefono"), tel => tel.matches("[0-9]+")))((n, d, t) => Cliente(n, d, t))((c: Cliente) => Some(c.nombre, c.direccion, c.telefono)))

  def clientes = Action.async { implicit rs =>
    clientesService.todos.map(clientes => Ok(views.html.clientes(clientes)))
  }

  def nuevoCliente = Action { implicit rs =>
    Ok(views.html.nuevoCliente(clienteForm))
  }

  def crearCliente = Action.async { implicit rs =>
    clienteForm.bindFromRequest.fold(
      fwe => Future.successful(BadRequest(views.html.nuevoCliente(fwe))),
      cliente =>
        clientesService.insertar(cliente).map { ic =>
          Redirect(routes.ClientesController.clientes()).flashing("success" -> Messages("cliente.creado"))
        })
  }
  
  def editarCliente(id: Int) = Action.async { implicit rs =>
    clientesService.getCliente(id).map{ 
      case None => Redirect(routes.ClientesController.clientes()).flashing("error" -> Messages("cliente.invalido"))
      case Some(cliente) =>
        Ok(views.html.editarCliente(id, clienteForm.fill(cliente)))
    }
  }

  def actualizar(id: Int) = Action.async { implicit rs =>
    clienteForm.bindFromRequest.fold(
      fwe => Future.successful(BadRequest(views.html.editarCliente(id, fwe))),
      cliente =>{
        clientesService.getCliente(id).flatMap{
          case None => Future.successful(Redirect(routes.ClientesController.clientes()).flashing("error" -> Messages("cliente.invalido")))
          case Some(clienteDB) => 
            clientesService.actualizar(id, cliente)
            .map(_ => Redirect(routes.ClientesController.clientes()).flashing("success" -> Messages("cliente.actualizado")))
        }
      }
    )
  }

  def eliminar(id: Int) = Action.async {
    clientesService.getCliente(id).flatMap { 
      case None => Future.successful(Redirect(routes.ClientesController.clientes()).flashing("error" -> Messages("cliente.invalido")))
      case Some(_) => 
        clientesService.eliminarCliente(id)
        .map(_ => Redirect(routes.ClientesController.clientes()).flashing("success" -> Messages("cliente.eliminado")))
    }
  }

  def getAction(implicit rs: Request[AnyContent]) = {
    rs.body.asFormUrlEncoded.get("action").headOption
  }

  def actualizarEliminarCliente(id: Int) = Action.async { implicit rs =>
     getAction match {
      case Some("delete") => eliminar(id)(rs)
      case _              => actualizar(id)(rs)
    }
  }

}