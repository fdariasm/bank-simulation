package controllers

import java.util.Date
import java.text.SimpleDateFormat
import java.text.NumberFormat
import java.util.Locale

object TemplateHelpers {
  
  val sf = new SimpleDateFormat("dd/MM/yyyy hh:mm ss")
  
  val locale = new Locale("es", "CO");      
  val currencyFormatter = NumberFormat.getCurrencyInstance(locale);
  
  def formatDate(date: Date) = sf.format(date)
  
  def formatCurrency(valor: Double) = currencyFormatter.format(valor)
    
}