package services

import java.text.SimpleDateFormat
import java.util.Date

import scala.concurrent.Future

import com.google.inject.ImplementedBy

import javax.inject.Inject
import models.Cliente
import models.Cuenta
import models.Movimiento
import models.Reporte
import models.TipoMovimiento
import repositories.CuentasRepository
import repositories.MovimientosRepository
import play.api.libs.concurrent.Execution.Implicits._

@ImplementedBy(classOf[CuentasServiceImp])
trait CuentasService {
  def todos(): Future[Seq[Cuenta]]

  def cuentasCliente(cliente: Cliente): Future[Seq[Cuenta]]

  def crearCuenta(cliente: Cliente): Future[Cuenta]

  def getCuenta(id: Int): Future[Option[Cuenta]]

  def eliminarCuenta(id: Int): Future[Int]

  def actualizar(id: Int, cuenta: Cuenta): Future[Int]
  
  def verReporte(cliente: Cliente, desde: Date, hasta: Date): Future[Seq[Reporte]]
}

class CuentasServiceImp @Inject() (
    val repository: CuentasRepository,
    val movimientosRepo: MovimientosRepository) extends CuentasService {

  val sf = new SimpleDateFormat("yyMMddkkmmssSS")

  def todos(): Future[Seq[Cuenta]] = repository.todos

  def cuentasCliente(cliente: Cliente): Future[Seq[Cuenta]] =
    repository.cuentasCliente(cliente)

  def crearCuenta(cliente: Cliente): Future[Cuenta] = {
    val cuenta = Cuenta(getNoRandom(cliente), 0, cliente.id.get)
    repository.insertar(cuenta)
  }

  def getNoRandom(cliente: Cliente): String = {
    s"${cliente.id.get}${sf.format(new Date)}"
  }

  def getCuenta(id: Int): Future[Option[Cuenta]] = repository.getCuenta(id)

  def eliminarCuenta(id: Int): Future[Int] = repository.eliminarCuenta(id)

  def actualizar(id: Int, cuenta: Cuenta): Future[Int] = repository.actualizar(id, cuenta)

  def verReporte(cliente: Cliente, desde: Date, hasta: Date): Future[Seq[Reporte]] = {
    repository.cuentasCliente(cliente).flatMap { cuentas =>
      Future.sequence {
        cuentas.map { cuenta =>
          movimientosRepo.movimientosCuenta(cuenta, desde, hasta).map { movimientos =>
            val totalDeb = getTotal(movimientos, TipoMovimiento.debito)
            val totalCred = getTotal(movimientos, TipoMovimiento.credito)
            Reporte(cuenta, totalDeb, totalCred)
          }
        }
      }
    }
  }

  def getTotal(mov: Seq[Movimiento], tipo: TipoMovimiento.TipoMovimiento): Double = {
    mov.filter(_.tipo == tipo).map(_.valor).sum
  }

}