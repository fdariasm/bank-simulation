package services

import scala.concurrent.Future
import javax.inject.Inject
import models.Cliente
import com.google.inject.ImplementedBy
import models.Cuenta
import repositories.CuentasRepository
import java.text.SimpleDateFormat
import java.util.Date
import models.Movimiento
import repositories.MovimientosRepository
import models.TipoMovimiento

@ImplementedBy(classOf[MovimientosServiceImp])
trait MovimientosService {
  def todos(): Future[Seq[Movimiento]]

  def movimientosCuenta(cuenta: Cuenta): Future[Seq[Movimiento]]

  def registrarMovimiento(
    cuenta: Cuenta,
    tipo: TipoMovimiento.TipoMovimiento,
    valor: Double): Either[String, Future[Movimiento]]
}

class MovimientosServiceImp @Inject() (
    val repository: MovimientosRepository) extends MovimientosService {

  def todos(): Future[Seq[Movimiento]] = repository.todos()

  def movimientosCuenta(cuenta: Cuenta): Future[Seq[Movimiento]] =
    repository.movimientosCuenta(cuenta)

  def registrarMovimiento(
    cuenta: Cuenta,
    tipo: TipoMovimiento.TipoMovimiento,
    valor: Double): Either[String, Future[Movimiento]] = {

    val movimiento = Movimiento(tipo, new Date, valor, cuenta.id.get)
    
    if (valor <= 0) Left(s"El valor no puede ser negativo $valor")
    else {
      tipo match {
        case TipoMovimiento.debito =>
          Right(repository.debitarCuenta(cuenta, movimiento))
        case TipoMovimiento.credito =>
          if ((cuenta.saldo - valor) < 0)
            Left(s"No hay fondos suficientes para registrar el movimiento $valor")
          else
            Right(repository.acreditarCuenta(cuenta, movimiento))
      }
    }
  }

}