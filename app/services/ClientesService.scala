package services

import scala.concurrent.Future
import javax.inject.Inject
import repositories.ClientesRepository
import models.Cliente
import com.google.inject.ImplementedBy

@ImplementedBy(classOf[ClientesServiceImp])
trait ClientesService {
  def todos(): Future[Seq[Cliente]]
  
  def insertar(cliente: Cliente): Future[Cliente] 
  
  def getCliente(id: Int): Future[Option[Cliente]]
  
  def eliminarCliente(id: Int): Future[Int]
  
  def actualizar(id: Int, cliente: Cliente): Future[Int]
}

class ClientesServiceImp @Inject() (
    val repository: ClientesRepository) extends ClientesService {
  
  def todos(): Future[Seq[Cliente]] = repository.todos()
  
  def insertar(cliente: Cliente): Future[Cliente] = repository.insertar(cliente)

  def getCliente(id: Int): Future[Option[Cliente]] = repository.getCliente(id)
  
  def eliminarCliente(id: Int): Future[Int] = repository.eliminarCliente(id)
  
  def actualizar(id: Int, cliente: Cliente): Future[Int] = repository.actualizar(id, cliente)
  
}