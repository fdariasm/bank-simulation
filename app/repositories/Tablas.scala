package repositories

import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import slick.driver.JdbcProfile
import models.Cliente
import models.Cuenta
import models.Movimiento
import java.util.Date
import models.TipoMovimiento

trait Tablas { this: HasDatabaseConfigProvider[JdbcProfile] =>
  import driver.api._

  def dbConfigProvider: DatabaseConfigProvider

  implicit val JavaUtilDateMapper =
    MappedColumnType.base[java.util.Date, java.sql.Timestamp](
      d => new java.sql.Timestamp(d.getTime),
      d => new java.util.Date(d.getTime))

  implicit def toTipoMovimiento(tipo: Int): TipoMovimiento.TipoMovimiento = TipoMovimiento.apply(tipo)

  implicit def fromTipoMovimiento(tipo: TipoMovimiento.TipoMovimiento): Int = tipo.id

  protected class ClientesTable(tag: Tag) extends Table[Cliente](tag, "cliente") {

    def nombre = column[String]("nombre")

    def direccion = column[String]("direccion")

    def telefono = column[String]("telefono")

    def id = column[Int]("id", O.AutoInc, O.PrimaryKey)

    def * = (nombre, direccion, telefono, id.?) <> (Cliente.tupled, Cliente.unapply _)
  }

  protected val clientes = TableQuery[ClientesTable]

  protected class CuentasTable(tag: Tag) extends Table[Cuenta](tag, "cuenta") {
    def numero = column[String]("numero")

    def saldo = column[Double]("saldo")

    def idCliente = column[Int]("id_cliente")

    def id = column[Int]("id", O.AutoInc, O.PrimaryKey)

    def * = (numero, saldo, idCliente, id.?) <> (Cuenta.tupled, Cuenta.unapply _)

    lazy val clienteFK = foreignKey("fk_cliente_cuenta", idCliente, clientes)(_.id)
  }

  protected val cuentas = TableQuery[CuentasTable]

  protected class MovimientosTable(tag: Tag) extends Table[Movimiento](tag, "movimiento") {
    def tipo = column[Int]("tipo")

    def fecha = column[Date]("fecha")

    def valor = column[Double]("valor")

    def idCuenta = column[Int]("id_cuenta")

    def id = column[Int]("id", O.AutoInc, O.PrimaryKey)

    def * = (tipo, fecha, valor, idCuenta, id.?) <> (toMovimiento, fromMovimiento)

    lazy val cuentaFK = foreignKey("fk_movimiento_cuenta", idCuenta, cuentas)(_.id)

    def toMovimiento(i: (Int, Date, Double, Int, Option[Int])): Movimiento = {
      Movimiento(i._1, i._2, i._3, i._4, i._5)
    }

    def fromMovimiento(mov: Movimiento): Option[(Int, Date, Double, Int, Option[Int])] = {
      Some(mov.tipo.id, mov.fecha, mov.valor, mov.idCuenta, mov.id)
    }
  }

  protected val movimientos = TableQuery[MovimientosTable]

}