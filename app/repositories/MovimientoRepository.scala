package repositories

import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile
import scala.concurrent.Future
import models.Movimiento
import com.google.inject.ImplementedBy
import models.Cuenta
import play.api.Logger
import java.util.Date

@ImplementedBy(classOf[MovimientosRepositoryImp])
trait MovimientosRepository {

  def todos(): Future[Seq[Movimiento]]

  def movimientosCuenta(cuenta: Cuenta): Future[Seq[Movimiento]]
  
  def movimientosCuenta(cuenta: Cuenta, desde: Date, hasta: Date): Future[Seq[Movimiento]]

  def acreditarCuenta(cuenta: Cuenta, movimiento: Movimiento): Future[Movimiento]

  def debitarCuenta(cuenta: Cuenta, movimiento: Movimiento): Future[Movimiento]
}

class MovimientosRepositoryImp @Inject() (
    val dbConfigProvider: DatabaseConfigProvider) extends MovimientosRepository with HasDatabaseConfigProvider[JdbcProfile] with Tablas {

  import driver.api._

  def todos(): Future[Seq[Movimiento]] = db.run(movimientos.result)

  def movimientosCuenta(cuenta: Cuenta): Future[Seq[Movimiento]] =
    db.run(movimientos.filter(_.idCuenta === cuenta.id.get).result)

  def debitarCuenta(cuenta: Cuenta, movimiento: Movimiento): Future[Movimiento] = db.run {
    val cuentaAct = cuenta.copy(saldo = (cuenta.saldo + movimiento.valor))
    doInsert(cuentaAct, movimiento).transactionally
  }

  def acreditarCuenta(cuenta: Cuenta, movimiento: Movimiento): Future[Movimiento] = db.run {
    val cuentaAct = cuenta.copy(saldo = (cuenta.saldo - movimiento.valor))
    doInsert(cuentaAct, movimiento).transactionally
  }

  def doInsert(cuenta: Cuenta, movimiento: Movimiento) = {
    Logger.info("Insertando movimiento: " + movimiento)
    val retId = movimientos.returning(movimientos.map(_.id)) += movimiento

    val insMov = retId.map { id => movimiento.copy(id = Some(id)) }

    val actCuenta = cuentas.filter(_.id === cuenta.id.get).update(cuenta)

    actCuenta.andThen(insMov)
  }
  
  def movimientosCuenta(cuenta: Cuenta, desde: Date, hasta: Date): Future[Seq[Movimiento]] = db.run{
    movimientos.filter(
        m => m.idCuenta === cuenta.id.get 
          && m.fecha >= desde && m.fecha <= hasta).result
  }

}