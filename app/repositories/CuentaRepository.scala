package repositories

import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile
import models.Cliente
import scala.concurrent.Future
import models.Movimiento
import models.Cuenta
import com.google.inject.ImplementedBy
import java.util.Date
import models.Reporte

@ImplementedBy(classOf[CuentasRepositoryImp])
trait CuentasRepository {
  def todos(): Future[Seq[Cuenta]]
  
  def cuentasCliente(cliente: Cliente): Future[Seq[Cuenta]]

  def insertar(cuenta: Cuenta): Future[Cuenta]
  
  def getCuenta(id: Int): Future[Option[Cuenta]] 
  
  def eliminarCuenta(id: Int): Future[Int]
  
  def actualizar(id: Int, cuenta: Cuenta): Future[Int]  
}

class CuentasRepositoryImp @Inject() (
    val dbConfigProvider: DatabaseConfigProvider) extends CuentasRepository with HasDatabaseConfigProvider[JdbcProfile] with Tablas {

  import driver.api._

  def todos(): Future[Seq[Cuenta]] = db.run(cuentas.result)
  
  def cuentasCliente(cliente: Cliente): Future[Seq[Cuenta]] = db.run{
    cuentas.filter(_.idCliente === cliente.id.get).result
  }

  def insertar(cuenta: Cuenta): Future[Cuenta] = db.run {
    val retId = cuentas.returning(cuentas.map(_.id)) += cuenta

    retId.map { id => cuenta.copy(id = Some(id)) }
  }
  
  def getCuenta(id: Int): Future[Option[Cuenta]] = db.run{
    cuentas.filter(_.id === id).result.headOption
  }

  def eliminarCuenta(id: Int): Future[Int] = db.run{
    cuentas.filter(_.id === id).delete
  }
  
  def actualizar(id: Int, cuenta: Cuenta): Future[Int] = db.run{
    cuentas.filter(_.id === id).update(cuenta)
  }
  
}