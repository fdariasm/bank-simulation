package repositories

import javax.inject.Inject
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile
import models.Cliente
import scala.concurrent.Future
import com.google.inject.ImplementedBy

@ImplementedBy(classOf[ClientesRepositoryImp])
trait ClientesRepository{
  def todos(): Future[Seq[Cliente]]
  
  def insertar(cliente: Cliente): Future[Cliente] 
  
  def getCliente(id: Int): Future[Option[Cliente]]
  
  def eliminarCliente(id: Int): Future[Int]
  
  def actualizar(id: Int, cliente: Cliente): Future[Int]
  
}

class ClientesRepositoryImp @Inject()(
    val dbConfigProvider: DatabaseConfigProvider) extends ClientesRepository with HasDatabaseConfigProvider[JdbcProfile] with Tablas{
  
  import driver.api._
    
  def todos(): Future[Seq[Cliente]] = db.run(clientes.result)
  
  def insertar(cliente: Cliente): Future[Cliente] = db.run{
    val retId = clientes.returning(clientes.map(_.id)) += cliente
    
    retId.map{id => cliente.copy(id = Some(id))}
  }
  
  def getCliente(id: Int): Future[Option[Cliente]] = db.run{
    clientes.filter(_.id === id).result.headOption
  }

  def eliminarCliente(id: Int): Future[Int] = db.run{
    clientes.filter(_.id === id).delete
  }
  
  def actualizar(id: Int, cliente: Cliente): Future[Int] = db.run{
    clientes.filter(_.id === id).update(cliente.copy(id = Some(id)))
  }
}